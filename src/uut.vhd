library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uut is
	generic(
		DATA_WIDTH : positive
	);
	port(
		clk_in : in  std_logic;
		ce_in  : in  std_logic;
		data_q : out std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end entity uut;

architecture RTL of uut is
	signal data : unsigned(data_q'range) := (others => '0');
begin

	data_q <= std_logic_vector(data);

	ctr_proc : process (clk_in)
	begin
		if rising_edge(clk_in) then
			if (ce_in = '0') then
				data <= (others => '0');
			else
				data <= data + 1;
			end if;
		end if;
	end process;
				

end architecture RTL;
